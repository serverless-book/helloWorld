exports.sayHello = (event, context, callback) => {
    const responseValue = {
        statusCode: 200,
        headers: {
            'Access-Control-Allow-Origin': '*',
        },
        body: 'Hello World',
    };
    callback(null, responseValue);
};